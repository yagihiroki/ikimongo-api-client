import API from '../index'
const apiURL = 'http://localhost:9292'
const validUser = {
  email: 'test@test.com',
  password: 'testtest'
}
const invalidUser = {
  email: 'hoge@hoge.hoge',
  password: 'hogehoge'
}
const loggedinClient = new API(apiURL)
beforeAll(() => {
  return loggedinClient.oauth(validUser)
})

describe('about auth', () => {
  const validClient = new API(apiURL)
  const invalidClient = new API(apiURL)
  test('login should be succeed', () => {
    return validClient.oauth(validUser)
  })

  test('login should be failed', () => {
    expect.assertions(1)
    return invalidClient.oauth(invalidUser)
      .catch(err => {
        expect(err.response.status).toBe(401)
      })
  })
});

describe('about collections', () => {
  test('can get collections', () => {
    expect.assertions(1)
    return loggedinClient.getCollections()
      .then(resp => {
        expect(resp.data).toBeInstanceOf(Array)
      })
  })

  test('can post & delete collection', async () => {
    let resp = await loggedinClient.postCollection({
      name: 'hello', statusID: 1, description: 'hello hello'})
    await loggedinClient.deleteCollectionByID(resp.data.id)
  })
})

describe('about users', () => {
  test('can get users', () => {
    return loggedinClient.getUsers()
  })

  test('can post & delete user', async () => {
    const client = new API(apiURL)
    const date = (new Date()).toISOString()
    let params = {
      name: 'yagi',
      email: `${date}@foo.com`,
      password: 'yagi0245',
      passwordConfirmation: 'yagi0245'
    }
    let resp = await client.postUser(params)
    await client.oauth({email: params.email, password: params.password})
    await client.deleteUserByID(resp.data.id)
  })
})

describe('about records', () => {
  test('can get records', async () => {
    expect.assertions(1)
    let resp = await loggedinClient.getRecords()
    console.log(resp.data)
    expect(resp.data).toBeInstanceOf(Array)
  })

  test('can post record', async () => {

  })
})
