import axios from 'axios'
import _ from 'lodash'

export default class {
  constructor (apiURL) {
    this.apiURL = apiURL
    this.token = null
    this.axios = axios.create({
      baseURL: apiURL,
      timeout: 10000
    })
  }

  oauth ({email, password}) {
    return new Promise((resolve, reject) => {
      this.axios.post('/oauth/token', {
        email: email,
        password: password,
        grant_type: 'password'
      }).then(resp => {
        this.isLogin = true
        this.token = resp.data.access_token
        this.axios.defaults.headers = {
          'Authorization': `Bearer ${resp.data.access_token}`
        }
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  }

  // GET /collections
  getCollections () {
    return this.axios.get('/collections.json')
  }
  // POST /collections
  postCollection ({name, statusID, description}) {
    let params = {
      collection: {
        name: name,
        status_id: statusID,
        description: description
      }
    }
    return this.axios.post('/collections', params)
  }
  // DELETE /collections/:id
  deleteCollectionByID (id) {
    return this.axios.delete(`/collections/${id}`)
  }

  // GET /users
  getUsers () {
    return this.axios.get('/users.json')
  }
  // POST /users
  postUser ({name, email, password, passwordConfirmation}) {
    let params = {
      user: {
        name: name,
        email: email,
        password: password,
        password_confirmation: passwordConfirmation
      }
    }
    return this.axios.post('/users', params)
  }
  // DELETE /users
  deleteUserByID (id) {
    return this.axios.delete(`/users/${id}`)
  }

  // GET  /records?collection_id=1
  getRecords (collectionID = []) {
    return this.axios.get('/records.json', {collection_id: collectionID})
  }
  // GET /records/:id
  getRecordByID (id) {
    return this.axios.get(`/records/${id}.json`)
    // parse additional data
      .then(resp => {
        let add = resp.data.additional_data
        if (add) {
          resp.data.additional_data = JSON.parse(add)
        }
        return resp
      })
  }
  // POST /records
  postRecord (record) {
    record = _.cloneDeep(record)
    if (record.additional_data) {
      record.additional_data = JSON.stringify(record.additional_data)
    }
    return this.axios.post('/records', {record: record})
  }
  // PUT /records
  updateRecordByID (id, record) {
    record = _.cloneDeep(record)
    if (record.additional_data) {
      record.additional_data = JSON.stringify(record.additional_data)
    }
    return this.axios.put(`/records/${id}`, {record: record})
  }
  // DELETE /records/:id
  deleteRecordByID (id) {
    return this.axios.delete(`/records/${id}`)
  }

  // GET /media
  getMedia (recordID) {
    let config = {
      responseType: 'blob',
      params: {record_id: recordID}
    }
    return this.axios.get('/media', config)
  }
  // POST /media
  postMedium (recordID, file) {
    let data = new FormData()
    data.append('medium[record_id]', recordID)
    data.append('medium[upload_file]', file)
    return this.axios.post('/media', data)
  }
}
